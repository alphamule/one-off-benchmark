var PouchDB = require('pouchdb');
const { PerformanceObserver, performance } = require('perf_hooks');

// set up performance observer
const obs = new PerformanceObserver((items) => {
  console.log(items.getEntries()[0].duration);
  performance.clearMarks();
});
obs.observe({ entryTypes: ['measure'] });

// set up db
performance.mark('setup start');
console.log('setting up database');
var db = new PouchDB('my_db');
performance.mark('setup end');
performance.measure('start up time', 'setup start', 'setup end');

// get db info
performance.mark('dbinfo start');
db.info().then(function (info) {
    performance.mark('dbinfo end');
    performance.measure('info time', 'dbinfo start', 'dbinfo end');
    console.log(info);
});

console.log('inserting doc');
var doc = {
  "_id": "mittens",
  "name": "Mittens",
  "occupation": "kitten",
  "age": 3,
  "hobbies": [
    "playing with balls of yarn",
    "chasing laser pointers",
    "lookin' hella cute"
  ]
};
performance.mark('put start');
db.put(doc);
performance.mark('put end');
performance.measure('put time', 'put start', 'put end');


console.log('retrieving doc');
performance.mark('get start');
db.get('mittens').then(function (doc) {
  console.log(doc);
  performance.mark('get end');
  performance.measure('get time', 'get start', 'get end');
});
 

