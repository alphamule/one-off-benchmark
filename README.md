A simple example of one way of doing benchmarking in a one-off node script with access to library dependencies.

To run, say `npm run one-off`

Docs on node perf\_hooks library: https://nodejs.org/api/perf_hooks.html

To see an example of debugging, say `npm run one-off-debug`

Then go to chrome and visit chrome://inspect and find your script.  You will then be able to use the browser debugger to debug your command line script.  From here, you will also be able to access profiling information.

